# cs437-lab04

### Part 1

`AWSCLI_IoT.sh` contains a series of Bash commands to use in AWS CloudShell to programatically create Things, attach principlals, and assign groups.

`lab4_emulator_cli.py` sends random weather data for 5 clients through an IoT Rule, which deposits that data into a Dynamo DB table.

### Part 2

`processEmissionsLambda` is the Lambda function running locally on the Greegrass Core to determine the max CO2 value for any given Vehicle Name. Zip the **contents of the folder** to create a deployment package. Utilizes the boto3 SDK to perform CRUD operations against a DynamoDB table of vehicle emission data.

`vehicleEmissions.py` enables a demo of inter-device pub-sub communication through CLI arguments.

`vehicleEmissions_v2.py` enables the publishing of vehicle emissions data, row-by-row for 5 vehicles to the Greengrass Core, where the Lambda subscriber determines the max emission value and publishes it back to the emulated vehicle upon each transmission.

### Part 3
`vehicle_emulator.py` publishes vehicle emission data, row by row, for each vehicle to an IoT topic that is then handled by an IoT Analytics channel + pipeline.
