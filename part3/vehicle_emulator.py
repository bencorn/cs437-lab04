# Import SDK packages
from email import message
from xmlrpc.client import DateTime
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import time
import json
import csv
import pandas as pd
import numpy as np
from datetime import datetime

device_st = 0
device_end = 4

certificate_formatter = "./9596b9709a35c1322b123e02eba5db5c2903a34803af3b3d71ccb6e49ac268bc-certificate.pem.crt"
key_formatter = "./9596b9709a35c1322b123e02eba5db5c2903a34803af3b3d71ccb6e49ac268bc-private.pem.key"

class MQTTClient:
    def __init__(self, device_id, cert, key):
        self.device_id = str(device_id)
        self.state = 0
        self.client = AWSIoTMQTTClient(self.device_id)
        self.client.configureEndpoint("akqt0eztqhrva-ats.iot.us-east-1.amazonaws.com", 8883)
        self.client.configureCredentials("./AmazonRootCA1.pem", key, cert)
        self.client.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
        self.client.configureDrainingFrequency(2)  # Draining: 2 Hz
        self.client.configureConnectDisconnectTimeout(10)  # 10 sec
        self.client.configureMQTTOperationTimeout(5)  # 5 sec
        self.client.onMessage = self.customOnMessage
        

    def customOnMessage(self,message):
        print("client {} received payload {} from topic {}".format(self.device_id, message.payload, message.topic))


    # Suback callback
    def customSubackCallback(self,mid, data):
        pass


    # Puback callback
    def customPubackCallback(self,mid):
        pass


    def publish(self, message, vehicle_id):        
        self.client.publishAsync(f"vehicle/data/{vehicle_id}", message, 0, ackCallback=self.customPubackCallback)


print("Initializing MQTTClients...")
clients = []
for device_id in range(device_st, device_end):
    client = MQTTClient(device_id,certificate_formatter.format(device_id,device_id) ,key_formatter.format(device_id,device_id))
    client.client.connect()
    clients.append(client)

def readVehicleData(vehicleId):
    dataArray = []
    fileName = f"./vehicle_data/{vehicleId}.csv"
    try:
        f = open(fileName, mode='r')
    except OSError:
        print(f"Error occurred when attempting to read file: {fileName}")
        return
    with f:
        csvReader = csv.DictReader(f)
        lineCount = 0
        for row in csvReader:
            data = {
                'vehicle_id': vehicleId,
                'timestamp': str(datetime.now()),
                'vehicle_CO': float(row['vehicle_CO']),
                'vehicle_CO2': float(row['vehicle_CO2']),
                'vehicleHC': float(row['vehicle_HC']),
                'vehicle_NOx': float(row['vehicle_NOx']),
                'vehicle_PMx': float(row['vehicle_PMx']),
                'vehicle_angle': float(row['vehicle_angle']),
                'vehicle_fuel': float(row['vehicle_fuel']),
                'vehicle_noise': float(row['vehicle_noise']),
                'vehicle_pos': float(row['vehicle_pos']),
                'vehicle_speed': float(row['vehicle_speed']),
                'vehicle_x': float(row['vehicle_x']),
                'vehicle_y': float(row['vehicle_y']),
            }
            dataArray.append(data)
            lineCount += 1
    return dataArray
 
while True:
    # For each vehicle, load the CSV file and publish each row as a message.
    # Pause for 1 second between publishing to simulate a real vehicle pushing
    # telemetry at some regular frequency.
    print("Preparing to publish vehicle data via MQTT")
    for i, c in enumerate(clients):
        print(f"Start publishing for Vehicle {i}")
        dataArray = readVehicleData(i)
        for data in dataArray:
            print(f"Sending data...")
            c.publish(json.dumps(data), i)
            time.sleep(1)
        time.sleep(60)

    print("Disconnecting clients")
    for c in clients:
        c.client.disconnect()

    print("All devices disconnected")
    exit()




