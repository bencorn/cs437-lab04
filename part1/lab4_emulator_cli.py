# Import SDK packages
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
import time
import json
import pandas as pd
import numpy as np
import random

#Starting and end index, modify this
device_st = 0
device_end = 100

#Path to your certificates, modify this
certificate_formatter = "9596b9709a35c1322b123e02eba5db5c2903a34803af3b3d71ccb6e49ac268bc-certificate.pem.crt"
key_formatter = "9596b9709a35c1322b123e02eba5db5c2903a34803af3b3d71ccb6e49ac268bc-private.pem.key"


class MQTTClient:
    def __init__(self, device_id, cert, key):
        # For certificate based connection
        self.device_id = str(device_id)
        self.state = 0
        self.client = AWSIoTMQTTClient(self.device_id)
        self.client.configureEndpoint("akqt0eztqhrva-ats.iot.us-east-1.amazonaws.com", 8883)
        self.client.configureCredentials("root.pem", key, cert)
        self.client.configureOfflinePublishQueueing(-1)  # Infinite offline Publish queueing
        self.client.configureDrainingFrequency(2)  # Draining: 2 Hz
        self.client.configureConnectDisconnectTimeout(10)  # 10 sec
        self.client.configureMQTTOperationTimeout(5)  # 5 sec
        self.client.onMessage = self.customOnMessage
        

    def customOnMessage(self,message):
        print("client {} received payload {} from topic {}".format(self.device_id, message.payload, message.topic))


    # Suback callback
    def customSubackCallback(self,mid, data):
        #You don't need to write anything here
        pass


    # Puback callback
    def customPubackCallback(self,mid):
        #You don't need to write anything here
        pass


    def publish(self, device_id, payload):
        #TODO4: fill in this function for your publish
        self.client.subscribeAsync(f"device/{device_id}/data", 0, ackCallback=self.customSubackCallback)
        
        self.client.publishAsync(f"device/{device_id}/data", payload, 0, ackCallback=self.customPubackCallback)

print("Initializing MQTTClients...")
clients = []
for device_id in range(device_st, device_end):
    client = MQTTClient(device_id,certificate_formatter.format(device_id,device_id) ,key_formatter.format(device_id,device_id))
    client.client.connect()
    clients.append(client)
 

while True:
    print("send now?")
    x = input()
    if x == "s":
        for i,c in enumerate(clients):
            payload = {
                "temperature": random.randint(-20,110),
                "humidity": random.randint(0,100),
                "barometer": random.randint(0,1100),
                "wind": {
                    "velocity": random.randint(0,200),
                    "bearing": random.randint(0,360)
                }
            }

            c.publish(i, json.dumps(payload))

    elif x == "d":
        for c in clients:
            c.client.disconnect()
        print("All devices disconnected")
        exit()
    else:
        print("wrong key pressed")

    time.sleep(3)





