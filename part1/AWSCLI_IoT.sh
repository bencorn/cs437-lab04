#!/bin/bash

for i in {1..100}
do
  aws iot create-thing \
      --thing-name "Thing$i"
done

thing_names=$(aws iot list-things | jq -r '.[][].thingName')

for thing in $thing_names
do
  aws iot attach-thing-principal \
      --thing-name $thing \
      --principal arn:aws:iot:us-east-1:809081790102:cert/9596b9709a35c1322b123e02eba5db5c2903a34803af3b3d71ccb6e49ac268bc
done

for thing in $thing_names
do
  aws iot add-thing-to-thing-group \
    --thing-name $thing \
    --thing-group-name Group
done
