import logging
import sys
import json
import greengrasssdk
import boto3
from decimal import *

client = greengrasssdk.client("iot-data")
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table("vehicle_emissions")

logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.INFO)

class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)
        return json.JSONEncoder.default(self, obj)

def lambda_handler(event, context):
    logger.info(json.dumps(event))

    thingName = event['thingName']
    data = event['data']
    maxValue = 0
    
    resp = table.get_item(
        Key = {
                'VehicleName' : thingName,
            }
        )
    
    if 'Item' in resp:
        if data > resp['Item']['MaxTemp']:
            resp = table.update_item(
                Key={
                    'VehicleName': thingName
                },
                UpdateExpression="set MaxTemp = :r",
                ExpressionAttributeValues={
                    ':r': Decimal(str(data)),
                },
                ReturnValues="UPDATED_NEW"
            )
            maxValue = data
        else:
            maxValue = resp['Item']['MaxTemp']
    else:
        vehicle = {
            'VehicleName': thingName,
            'MaxTemp': Decimal(str(data))
        }
        table.put_item(Item=vehicle)
        maxValue = data
        
    
    publishTopic = f"vehicle/emissions/response/{thingName}"

    logger.info(publishTopic)
    logger.info(maxValue)
    client.publish(topic=publishTopic, payload=json.dumps(maxValue, cls=JSONEncoder))