# /*
# * Copyright 2010-2017 Amazon.com, Inc. or its affiliates. All Rights Reserved.
# *
# * Licensed under the Apache License, Version 2.0 (the "License").
# * You may not use this file except in compliance with the License.
# * A copy of the License is located at
# *
# *  http://aws.amazon.com/apache2.0
# *
# * or in the "license" file accompanying this file. This file is distributed
# * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
# * express or implied. See the License for the specific language governing
# * permissions and limitations under the License.
# */

import os
import sys
import time
import uuid
import json
import logging
import argparse
import csv
from venv import create
import psutil
from AWSIoTPythonSDK.core.greengrass.discovery.providers import DiscoveryInfoProvider
from AWSIoTPythonSDK.core.protocol.connection.cores import ProgressiveBackOffCore
from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTClient
from AWSIoTPythonSDK.exception.AWSIoTExceptions import DiscoveryInvalidRequestException

MAX_DISCOVERY_RETRIES = 10
GROUP_CA_PATH = "./groupCA/"
HOST = 'akqt0eztqhrva-ats.iot.us-east-1.amazonaws.com'
VEHICLES = ['Vehicle0', 'Vehicle1', 'Vehicle2', 'Vehicle3', 'Vehicle4']

def addLoggingLevel(levelName, levelNum, methodName=None):
    """
    Comprehensively adds a new logging level to the `logging` module and the
    currently configured logging class.

    `levelName` becomes an attribute of the `logging` module with the value
    `levelNum`. `methodName` becomes a convenience method for both `logging`
    itself and the class returned by `logging.getLoggerClass()` (usually just
    `logging.Logger`). If `methodName` is not specified, `levelName.lower()` is
    used.

    To avoid accidental clobberings of existing attributes, this method will
    raise an `AttributeError` if the level name is already an attribute of the
    `logging` module or if the method name is already present 

    """
    if not methodName:
        methodName = levelName.lower()

    if hasattr(logging, levelName):
       raise AttributeError('{} already defined in logging module'.format(levelName))
    if hasattr(logging, methodName):
       raise AttributeError('{} already defined in logging module'.format(methodName))
    if hasattr(logging.getLoggerClass(), methodName):
       raise AttributeError('{} already defined in logger class'.format(methodName))

    def logForLevel(self, message, *args, **kwargs):
        if self.isEnabledFor(levelNum):
            self._log(levelNum, message, args, **kwargs)
    def logToRoot(message, *args, **kwargs):
        logging.log(levelNum, message, *args, **kwargs)

    logging.addLevelName(levelNum, levelName)
    setattr(logging, levelName, levelNum)
    setattr(logging.getLoggerClass(), methodName, logForLevel)
    setattr(logging, methodName, logToRoot)

#
# Configure logging
#
addLoggingLevel('PROGRESS', logging.DEBUG + 1)
logger = logging.getLogger(__name__)
logger.setLevel(logging.PROGRESS)
streamHandler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
streamHandler.setFormatter(formatter)
logger.addHandler(streamHandler)

#
# Setup core back off
#
backOffCore = ProgressiveBackOffCore()

#
# Create discovery info provider
#
def createDiscoveryInfoProvider(host, rootCAPath, certificatePath, privateKeyPath):
    discoveryInfoProvider = DiscoveryInfoProvider()
    discoveryInfoProvider.configureEndpoint(host)
    discoveryInfoProvider.configureCredentials(rootCAPath, certificatePath, privateKeyPath)
    discoveryInfoProvider.configureTimeout(10)  # 10 sec
    return discoveryInfoProvider

#
# Acquire discovery info, return core info and groupCA
#
def acquireDiscoveryInfo(discoveryInfoProvider, vehicleName):
    retryCount = MAX_DISCOVERY_RETRIES
    discovered = False
    groupCA = None
    coreInfo = None
    while retryCount != 0:
        try:
            discoveryInfo = discoveryInfoProvider.discover(vehicleName)
            caList = discoveryInfo.getAllCas()
            coreList = discoveryInfo.getAllCores()

            # We only pick the first ca and core info
            groupId, ca = caList[0]
            coreInfo = coreList[0]
            logger.debug("Discovered GGC: %s from Group: %s" % (coreInfo.coreThingArn, groupId))

            logger.debug("Now we persist the connectivity/identity information...")
            groupCA = GROUP_CA_PATH + groupId + "_CA_" + str(uuid.uuid4()) + ".crt"
            if not os.path.exists(GROUP_CA_PATH):
                os.makedirs(GROUP_CA_PATH)
            groupCAFile = open(groupCA, "w")
            groupCAFile.write(ca)
            groupCAFile.close()

            discovered = True
            logger.debug("Now proceed to the connecting flow...")
            break
        except DiscoveryInvalidRequestException as e:
            logger.debug("Invalid discovery request detected!")
            logger.debug("Type: %s" % str(type(e)))
            logger.debug("Error message: %s" % str(e))
            logger.debug("Stopping...")
            break
        except BaseException as e:
            logger.debug("Error in discovery!")
            logger.debug("Type: %s" % str(type(e)))
            logger.debug("Error message: %s" % str(e))
            retryCount -= 1
            logger.debug("\n%d/%d retries left\n" % (retryCount, MAX_DISCOVERY_RETRIES))
            logger.debug("Backing off...\n")
            backOffCore.backOff()

    if not discovered:
        logger.debug("Discovery failed after %d retries. Exiting...\n" % (MAX_DISCOVERY_RETRIES))
        sys.exit(-1)

    return coreInfo, groupCA

#
# Create MQTT client, return the client
#
def createMQTTClient(vehicleName, groupCA, privateKeyPath, certificatePath, coreInfo):
    # Iterate through all connection options for the core and use the first successful one
    MQTTClient = AWSIoTMQTTClient(vehicleName)
    MQTTClient.configureCredentials(groupCA, privateKeyPath, certificatePath)

    connected = False
    for connectivityInfo in coreInfo.connectivityInfoList:
        currentHost = connectivityInfo.host
        currentPort = connectivityInfo.port
        logger.debug("Trying to connect to core at %s:%d" % (currentHost, currentPort))
        MQTTClient.configureEndpoint(currentHost, currentPort)
        try:
            MQTTClient.connect()
            connected = True
            break
        except BaseException as e:
            logger.debug("Error in connect!")
            logger.debug("Type: %s" % str(type(e)))
            logger.debug("Error message: %s" % str(e))

    if not connected:
        logger.debug("Cannot connect to core %s. Exiting..." % coreInfo.coreThingArn)
        sys.exit(-2)

    return MQTTClient

#
# Retrieve CO2 data as float array for specified vehicle
#
def readVehicleData(vehicleName):
    data = []
    fileName = f"./vehicle_data/{vehicleName}.csv"
    try:
        f = open(fileName, mode='r')
    except OSError:
        logger.debug(f"Error occurred when attempting to read file: {fileName}")
        return
    with f:
        csv_reader = csv.DictReader(f)
        for row in csv_reader:
            data.append((float(row['vehicle_CO2'])))
    return data

#
# Handle New Message on Subscribed Topic
#
def onMessageHandler(self, mid, data):
    payload = json.loads(data.payload)
    formattedMessage = f"Message received on: {data.topic}: {payload}"
    logger.progress(formattedMessage)

#
# Main Driver
#
for v in VEHICLES:
    logger.progress("Starting publish for %s" % (v))
    nameLower = v.lower()
    rootCA = 'root.pem'
    cert = f'keys/{nameLower}/{nameLower}.cert.pem'
    privateKey = f'keys/{nameLower}/{nameLower}.private.key'
    subscribeTopic = f"vehicle/emissions/response/{v}"
    publishTopic = f"vehicle/emissions/{v}"

    discoveryInfoProvider = createDiscoveryInfoProvider(HOST, rootCA, cert, privateKey)
    coreInfo, groupCA = acquireDiscoveryInfo(discoveryInfoProvider, v)
    MQTTClient = createMQTTClient(v, groupCA, privateKey, cert, coreInfo)

    MQTTClient.subscribe(subscribeTopic, 0, onMessageHandler)

    data = readVehicleData(v.lower())
    length = len(data)

    data = data[0:10]
    for i, d in enumerate(data):

        message = {
            'thingName': v,
            'data': d,
        }
        messageJson = json.dumps(message)
        MQTTClient.publish(publishTopic, messageJson, 0)
        logger.progress('Published data point %i of %i to topic %s: %s\n' % (i, length, publishTopic, messageJson))
        time.sleep(1)

    time.sleep(5)